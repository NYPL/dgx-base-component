# DGX Base Component

This is the repository for the component which serves as the foundation 
for other components to be built on. It contains general methods and 
properties that are needed by all the components.

## Usage

    import BaseComponent from 'dgx-base-component';

	<BaseComponent content={YourContent} />

## Props

- `id`: ID of the base component (String, default: "BaseComponent")
- `className`: The class name of the base component (String, default: BaseComponent")
- `content` : The data of the base component. It is not used for now. Every component which is extended from the base component should over write this and has its own content prop. (String, default: "")
- `lang`: Language. Not used, but provided for future internationalization
  (String, default: "en")

## Demo

After cloning the repo:

    npm install
    npm start

Then load [http://localhost:3000](http://localhost:3000) in your browser.