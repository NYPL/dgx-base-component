// Import libraries
import React from 'react';

class BaseComponent extends React.Component {

  constructor(props) {
    super(props);

    this.state = {};

    this._bind = this._bind.bind(this);
  }

  /**
   * _bind(method)
   * This function is to bind the methods all at once for the cpomponents we
   * use in this app.
   *
   * @param {String} name of method
   */
  _bind(...methods) {
    methods.forEach((method) => {
      this[method] = this[method].bind(this);
    });
  }

  render() {
    return (
      <div>{this.props.content}</div>
    );
  }
}

BaseComponent.propTypes = {
  content: React.PropTypes.string,
};

BaseComponent.defaultProps = {
  content: '',
};

export default BaseComponent;
