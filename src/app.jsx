import React from 'react';
import BaseComponent from './components/BaseComponent/BaseComponent.jsx';

/* app.jsx
 * Used for local development of React Components
 */
React.render(React.createElement(BaseComponent), document.body);
